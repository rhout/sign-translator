# Text to sign language translator :v:

The very under styled app for translating latin characters to sign language "characters".

## Get started

Go to the project directory in a terminal.

### Install dependencies

`npm i`

### Run the application

`npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
