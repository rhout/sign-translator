const { Link } = require("react-router-dom");

const NotFound = () => {
    return (
        <div>
            <h1>404: This is not a valid path...</h1>
            <Link to="/">Go home</Link>
        </div>
    );
};

export default NotFound;
