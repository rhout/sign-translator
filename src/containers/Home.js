import styled from "styled-components";
import { useHistory } from "react-router-dom";

import Login from "../components/home/Login";
import WelcomeText from "../components/home/WelcomeText";

const HomePropTypes = {};

const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const Home = () => {
    const history = useHistory();
    const loggedIn = localStorage.getItem("name") || "";
    if (loggedIn) {
        history.push("/translate");
    }
    const handleLogin = (name) => {
        localStorage.setItem("name", name);
        history.push("/translate");
    };

    return (
        <Container>
            <WelcomeText />
            <Login onEnterPressed={handleLogin} onLogin={handleLogin} />
        </Container>
    );
};

Home.propTypes = HomePropTypes;

export default Home;
