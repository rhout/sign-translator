import { useHistory } from "react-router-dom";

import StoredSentences from "../components/profile/StoredSentences";
import UserProfile from "../components/profile/UserProfile";

const ProfilePropTypes = {};

const Profile = () => {
    const history = useHistory();

    let storedSentences = localStorage.getItem("sentences") || "";
    const name = localStorage.getItem("name") || "";

    const doLogout = () => {
        localStorage.removeItem("name");
        localStorage.removeItem("sentences");
        history.push("/");
    };

    return (
        <div>
            <UserProfile history={history} name={name} onLogout={doLogout} />
            <h3>The last ten translations:</h3>
            <StoredSentences storedSentences={storedSentences} />
        </div>
    );
};

Profile.propTypes = ProfilePropTypes;

export default Profile;
