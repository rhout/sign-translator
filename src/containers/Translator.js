import { useState } from "react";
import PropTypes from "prop-types";

import TranslateInput from "../components/translator/TranslateInput";
import TranslatedBox from "../components/translator/TranslatedBox";
import ProfileAvatar from "../components/translator/ProfileAvatar";
import styled from "styled-components";

const TranslatorPropTypes = {
    history: PropTypes.object.isRequired,
};

const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const InputProfileContainer = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    justify-content: flex-end;
    cursor: pointer;
`;

const Translator = ({ history }) => {
    const [sentence, setSentence] = useState("");

    const name = (localStorage.getItem("name") || "").trim();
    if (!name) {
        history.goBack();
    }

    const translate = (sentenceToTranslate) => {
        updateStoredSentences(sentenceToTranslate);
        setSentence(sentenceToTranslate);
    };

    const goToProfilePage = () => {
        history.push("/profile");
    };

    const updateStoredSentences = (sentenceToInsert) => {
        const savedSentences = (localStorage.getItem("sentences") || "").split(
            ","
        );

        if (savedSentences.length > 9) {
            savedSentences.pop();
        }
        savedSentences.unshift(sentenceToInsert);

        localStorage.setItem("sentences", savedSentences);
    };

    return (
        <Container>
            <InputProfileContainer>
                <TranslateInput onTranslate={translate} />
                <ProfileAvatar
                    userName={name}
                    onGoToProfile={goToProfilePage}
                />
            </InputProfileContainer>
            <TranslatedBox sentence={sentence} />
        </Container>
    );
};

Translator.propTypes = TranslatorPropTypes;

export default Translator;
