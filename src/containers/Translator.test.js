import renderer from "react-test-renderer";
import Translator from "./Translator";

it("renders correctly", () => {
    const history = {
        goBack: jest.fn(),
    };
    const tree = renderer.create(<Translator history={history} />).toJSON();
    expect(tree).toMatchSnapshot();
});
