import renderer from "react-test-renderer";
import Profile from "./Profile";

it("renders correctly", () => {
    const tree = renderer.create(<Profile />).toJSON();
    expect(tree).toMatchSnapshot();
});
