import Input from "./Input";
import { render, fireEvent } from "@testing-library/react";

const onEnterPressedMock = jest.fn();
const onChangeMock = jest.fn();
let input;

beforeAll(() => {
    const { getByPlaceholderText } = render(
        <Input
            placeholder="Enter text"
            onEnterPressed={onEnterPressedMock}
            onChange={onChangeMock}
        />
    );

    input = getByPlaceholderText("Enter text");
});

beforeEach(() => {
    onChangeMock.mockClear();
    onEnterPressedMock.mockClear();
});

test("Has correct placeholder text", () => {
    expect(input).toBeTruthy();
});

describe("callbacks are called", () => {
    // const { getByTestId } = render(
    //     <Input
    //         placeholder="Enter text"
    //         onEnterPressed={onEnterPressedMock}
    //         onChange={onChangeMock}
    //     />
    // );

    // const input = getByTestId("input");
    test("is calling onEnterPressed, when enter is pressed", () => {
        fireEvent.keyUp(input, { key: "Enter", code: "Enter" });

        // input.handleOnKeyUp({ keyCode: 13 });

        // expect(onEnterPressedMock).toBeCalled();
    });

    test("is not calling onEnterPressed, when a non-enter key is pressed", () => {
        fireEvent.keyUp(input, { key: "A", code: "KeyA" });

        // expect(onEnterPressedMock).toBeCalled();
    });

    test("is calling onChange, when a character is entered", () => {
        fireEvent.keyDown(input, { key: "A", code: "KeyA" });

        // expect(onChangeMock).toBeCalled();
    });
});
