import PropTypes from "prop-types";
import styled from "styled-components";
import getSignsFromSentence from "../../utils/lettersToSignTranslator";

const TranslatedBoxPropTypes = {
    sentence: PropTypes.string,
};

const OuterContainer = styled.div`
    margin-top: 3%;
    min-width: 500px;
    height: 300px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    border: black solid 1px;
    padding: 2%;
    overflow: overlay;
`;

const SignsContainer = styled.div`
    padding: 0 2%;
`;

const SignImage = styled.img`
    height: 40px;
`;

const renderLettersInWord = (word) =>
    word.map((letter, index) => <SignImage src={letter} key={index} />);

const renderAllSigns = (words) => {
    return words.map((word, index) => {
        const lettersInWords = renderLettersInWord(word);
        return <SignsContainer key={index}>{lettersInWords}</SignsContainer>;
    });
};

const TranslatedBox = ({ sentence }) => {
    const signsFromSentence = getSignsFromSentence(sentence);
    return <OuterContainer>{renderAllSigns(signsFromSentence)}</OuterContainer>;
};

TranslatedBox.propTypes = TranslatedBoxPropTypes;

export default TranslatedBox;
