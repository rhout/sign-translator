import { fireEvent, render } from "@testing-library/react";
import renderer from "react-test-renderer";
import TranslateInput from "./TranslateInput";

it("renders correctly", () => {
    const tree = renderer
        .create(<TranslateInput onTranslate={jest.fn()} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

describe("translate button", () => {
    const { getByRole } = render(<TranslateInput onTranslate={jest.fn()} />);

    const nameInput = getByRole("textbox");
    const button = getByRole("button");
    test("is disabled when there is no or only whitespace input", () => {
        expect(nameInput.value).toBe("");
        expect(button).toBeDisabled();
    });

    test("is enabled when an input is given", () => {
        const { getByRole, getByPlaceholderText } = render(
            <TranslateInput onTranslate={jest.fn()} />
        );

        const nameInput = getByRole("textbox");
        const button = getByRole("button");

        fireEvent.change(getByPlaceholderText("Write something"), {
            target: { value: "Rune" },
        });
        expect(nameInput.value).toBe("Rune");
        expect(button).toBeEnabled();
    });
});
