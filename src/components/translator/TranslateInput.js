import { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import PrimaryButton from "../PrimaryButton";
import Input from "../Input";

const TranslateInputProptypes = {
    onTranslate: PropTypes.func.isRequired,
};

const Container = styled.div`
    display: flex;
`;

const TranslateInput = ({ onTranslate }) => {
    const [sentence, setSentence] = useState("");

    const doTranslate = () => {
        onTranslate(sentence);
    };
    return (
        <Container>
            <Input
                placeholder="Write something"
                onEnterPressed={doTranslate}
                onChange={(value) => setSentence(value)}
            />
            <PrimaryButton
                text="Translate!"
                disabled={!sentence.trim()}
                onClick={doTranslate}
            />
        </Container>
    );
};

TranslateInput.propTypes = TranslateInputProptypes;

export default TranslateInput;
