import { render } from "@testing-library/react";
import renderer from "react-test-renderer";
import ProfileAvatar from "./ProfileAvatar";

test("renders correctly", () => {
    const tree = renderer
        .create(<ProfileAvatar userName="Rune" onGoToProfile={jest.fn()} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test("Shows the name of the user", () => {
    const { getByRole } = render(
        <ProfileAvatar onGoToProfile={jest.fn()} userName={"Rune"} />
    );

    const header = getByRole("heading", { level: 3 });

    expect(header.innerHTML).toBe("Rune");
});
