import PropTypes from "prop-types";
import styled from "styled-components";

import avatar from "../../assets/avatar.png";

const ProfileAvatarPropTypes = {
    userName: PropTypes.string.isRequired,
    onGoToProfile: PropTypes.func.isRequired,
};

const Container = styled.div`
    display: flex;
    align-items: center;
    align-self: flex-end;
    padding-left: 10%;
`;

const UserName = styled.h3`
    margin-right: 12px;
`;

const Avatar = styled.img`
    background: white;
    border-radius: 50%;
    height: 40px;
`;

const ProfileAvatar = ({ userName, onGoToProfile }) => {
    const firstName = userName.split(" ")[0];
    return (
        <Container onClick={onGoToProfile}>
            <UserName>{firstName}</UserName>
            <Avatar src={avatar} />
        </Container>
    );
};

ProfileAvatar.propTypes = ProfileAvatarPropTypes;

export default ProfileAvatar;
