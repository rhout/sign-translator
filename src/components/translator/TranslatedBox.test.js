import { getAllByRole, render } from "@testing-library/react";
import renderer from "react-test-renderer";
import TranslatedBox from "./TranslatedBox";

it("renders correctly", () => {
    const tree = renderer
        .create(<TranslatedBox sentence="This is a sentence" />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test("'Word' gives 4 sign images", () => {
    const { getAllByRole } = render(<TranslatedBox sentence="Word" />);

    const signs = getAllByRole("img");

    expect(signs.length).toBe(4);
});

test("'This is a sentence' gives 15 sign images", () => {
    const { getAllByRole } = render(
        <TranslatedBox sentence="This is a sentence" />
    );

    const signs = getAllByRole("img");

    expect(signs.length).toBe(15);
});
