import React from "react";
import PropTypes from "prop-types";

const InputPropTypes = {
    placeholder: PropTypes.string.isRequired,
    onEnterPressed: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
};

const Input = ({ placeholder, onEnterPressed, onChange }) => {
    const handleOnChange = (event) => {
        onChange(event.target.value);
    };

    const handleOnKeyUp = (event) => {
        if (event.keyCode === 13) {
            onEnterPressed();
        }
    };

    return (
        <input
            placeholder={placeholder}
            onKeyUp={handleOnKeyUp}
            onChange={handleOnChange}
        />
    );
};

Input.propTypes = InputPropTypes;

export default Input;
