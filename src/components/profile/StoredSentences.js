import PropTypes from "prop-types";
import styled from "styled-components";

const StoredSentencesPropTypes = {
    storedSentences: PropTypes.string.isRequired,
};

const ListItem = styled.li``;

const List = styled.ol``;

/**
 *
 * @param {String} storedSentences Creates a list item for each of the stored sentences for the current user.
 */
const renderStoredSentences = (storedSentences) => {
    storedSentences = storedSentences ? storedSentences.split(",") : [];
    return storedSentences.map((sentence, index) => {
        return <ListItem key={index}>{sentence}</ListItem>;
    });
};

/**
 * Displays a list of the last ten translations made for the current user.
 */
const StoredSentences = ({ storedSentences }) => {
    return <List>{renderStoredSentences(storedSentences)}</List>;
};

StoredSentences.propTypes = StoredSentencesPropTypes;

export default StoredSentences;
