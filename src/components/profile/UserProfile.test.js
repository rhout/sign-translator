import renderer from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import UserProfile from "./UserProfile";

it("renders correctly", () => {
    const tree = renderer
        .create(<UserProfile onLogout={jest.fn()} name={"Rune"} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test("Shows the name of the user", () => {
    const { getByRole } = render(
        <UserProfile onLogout={jest.fn()} name={"Rune"} />
    );

    const header = getByRole("heading", { level: 3 });

    expect(header.innerHTML).toBe("Name: Rune");
});
