import PropTypes from "prop-types";
import styled from "styled-components";
import { useHistory } from "react-router-dom";

import backIcon from "../../assets/back.png";
import PrimaryButton from "../PrimaryButton";

const UserProfileProptypes = {
    name: PropTypes.string,
    onLogout: PropTypes.func.isRequired,
};

const Container = styled.div`
    display: flex;
    align-items: center;
`;

const UserText = styled.h3`
    margin-left: 20px;
    margin-right: 5px;
`;

const BackImage = styled.img`
    background: none;
    border: none;
    cursor: pointer;
    &:focus {
        outline: none;
    }
`;

const UserProfile = ({ name, onLogout }) => {
    const history = useHistory();
    return (
        <Container>
            <BackImage src={backIcon} onClick={() => history.goBack()} />
            <UserText>Name: {name}</UserText>
            <PrimaryButton text="Log out" onClick={onLogout} />
        </Container>
    );
};

UserProfile.propTypes = UserProfileProptypes;

export default UserProfile;
