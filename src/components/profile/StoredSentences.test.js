import renderer from "react-test-renderer";
import { render } from "@testing-library/react";
import StoredSentences from "./StoredSentences";

test("renders correctly", () => {
    const tree = renderer
        .create(<StoredSentences storedSentences="this,is,a,sentence" />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test("5 sentences gives 5 list items ", () => {
    const { getByRole } = render(
        <StoredSentences storedSentences="this,is,an,informative,sentence" />
    );

    const list = getByRole("list");

    expect(list.children.length).toBe(5);
});
