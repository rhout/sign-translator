import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const PrimaryButtonPropTypes = {
    text: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
};

const Button = styled.button`
    color: white;
    background-color: #2185d0;
    width: 100px;
    padding: 10px;
    margin-left: 10px;
    border: none;
    border-radius: 2px;
    cursor: pointer;
    &:hover {
        background-color: #1678c2;
    }
    &:active {
        background-color: #1a69a4;
    }
    &:disabled {
        background-color: gray;
        cursor: default;
    }
    &:focus {
        outline: none;
    }
`;

function PrimaryButton({ text, disabled, onClick }) {
    return (
        <Button onClick={onClick} disabled={disabled}>
            {text}
        </Button>
    );
}

PrimaryButton.propTypes = PrimaryButtonPropTypes;

export default PrimaryButton;
