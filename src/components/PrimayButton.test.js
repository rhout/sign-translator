import Button from "./PrimaryButton";
import { render } from "@testing-library/react";

test("text in button is as expected", () => {
    const { rerender, getByRole } = render(
        <Button onClick={jest.fn()} text="Test" />
    );

    const button = getByRole("button");
    expect(button.innerHTML).toBe("Test");

    rerender(<Button onClick={jest.fn()} text="Rune" />);
    expect(button.innerHTML).toBe("Rune");
});
