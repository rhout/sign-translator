import { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import PrimaryButton from "../PrimaryButton";

const LoginPropTypes = {
    onLogin: PropTypes.func.isRequired,
    onEnterPressed: PropTypes.func.isRequired,
};

const Container = styled.div`
    display: flex;
`;

const Login = ({ onLogin, onEnterPressed }) => {
    const [name, setName] = useState("");

    const onKeyPressed = (e) => {
        if (e.keyCode === 13) {
            onEnterPressed(name);
        }
    };

    const doLogin = () => {
        onLogin(name);
    };

    return (
        <Container>
            <input
                placeholder="Enter your name"
                onKeyUp={onKeyPressed}
                onChange={(e) => setName(e.target.value)}
            />
            <PrimaryButton
                text="Go!"
                disabled={!name.trim()}
                onClick={doLogin}
            />
        </Container>
    );
};

Login.propTypes = LoginPropTypes;

export default Login;
