import renderer from "react-test-renderer";
import Login from "./Login";
import { render, fireEvent } from "@testing-library/react";

test("renders correctly", () => {
    const tree = renderer
        .create(<Login onEnterPressed={jest.fn()} onLogin={jest.fn()} />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

describe("login button", () => {
    test("is disabled when there is no or only whitespace input", () => {
        const { getByText, getByPlaceholderText } = render(
            <Login onEnterPressed={jest.fn()} onLogin={jest.fn()} />
        );

        const nameInput = getByPlaceholderText(/Enter your name/i);
        const button = getByText(/Go!/i);

        expect(nameInput.value).toBe("");
        expect(button).toBeDisabled();
    });

    test("is enabled when an input is given", () => {
        const { getByText, getByPlaceholderText } = render(
            <Login onEnterPressed={jest.fn()} onLogin={jest.fn()} />
        );

        const nameInput = getByPlaceholderText(/Enter your name/i);
        const button = getByText(/Go!/i);

        fireEvent.change(getByPlaceholderText("Enter your name"), {
            target: { value: "Rune" },
        });
        expect(nameInput.value).toBe("Rune");
        expect(button).toBeEnabled();
    });
});
