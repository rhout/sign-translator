const WelcomeText = () => {
    return (
        <h3>
            Welcome to the text to sign language translator. Please enter your
            name to continue.
        </h3>
    );
};

export default WelcomeText;
