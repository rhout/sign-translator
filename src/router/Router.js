import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "../containers/Home";
import Profile from "../containers/Profile";
import Translator from "../containers/Translator";
import NotFound from "../containers/NotFound";

const router = () => (
    <Router>
        <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/translate" component={Translator} />
            <Route path="/profile" component={Profile} />
            <Route path="*" component={NotFound} />
        </Switch>
    </Router>
);

export default router;
