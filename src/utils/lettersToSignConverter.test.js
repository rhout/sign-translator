import lettersToSigns from "./lettersToSignTranslator";

import Signs from "../assets/individial_signs";

test("Renders the 'a' sign", () => {
    const expected = [[Signs.a]];
    expect(lettersToSigns("a")).toEqual(expected);
});

test("Ignores casing", () => {
    expect(lettersToSigns("a")).toEqual(lettersToSigns("A"));
});

test("Will render one sign", () => {
    const expected = [[Signs.r, Signs.u, Signs.n, Signs.e]];
    expect(lettersToSigns("rune")).toEqual(expected);
});

test("Strips non letter characters", () => {
    const expected = [
        [Signs.h, Signs.i],
        [Signs.i],
        [Signs.a, Signs.m],
        [Signs.r, Signs.u, Signs.n, Signs.e],
    ];
    expect(lettersToSigns("Hi, I am Rune!")).toEqual(expected);
});

test("Can handle zero input, null and undefined", () => {
    const expected = [];
    expect(lettersToSigns("")).toEqual(expected);
    expect(lettersToSigns(null)).toEqual(expected);
    expect(lettersToSigns(undefined)).toEqual(expected);
});
