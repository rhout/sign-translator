import Signs from "../assets/individial_signs";

const convertToCleanWordArray = (sentence) => {
    return sentence
        ? sentence
              .replace(/[^a-zA-Z ]/g, "")
              .toLowerCase()
              .split(" ")
        : [];
};

const translateWord = (lettersInWord) => {
    const signsFromLetters = [];
    for (const letter of lettersInWord) {
        signsFromLetters.push(Signs[letter]);
    }
    return signsFromLetters;
};

const createWordArrays = (words) => {
    const wordSigns = [];
    for (const word of words) {
        const lettersInWord = word.split("");
        const wordAsSigns = translateWord(lettersInWord);
        wordSigns.push(wordAsSigns);
    }

    return wordSigns;
};

const translateSentence = (sentence) => {
    const words = convertToCleanWordArray(sentence);
    return createWordArrays(words);
};

export default translateSentence;
